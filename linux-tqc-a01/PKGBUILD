# AArch64 kernel for TQC A01
# Maintainer: yjun <jerrysteve1101 at gmail dot com>

# patches from Armbian and libreELEC
# https://github.com/LibreELEC/LibreELEC.tv/blob/master/projects/Allwinner/patches/linux/
# https://github.com/armbian/build/tree/main/patch/kernel/archive/sunxi-6.11/

pkgbase=linux-tqc-a01
_srcname=linux-6.11
_kernelname=${pkgbase#linux}
_desc="AArch64 kernel for TQC A01"
pkgver=6.11.5
pkgrel=1
arch=('aarch64')
url="http://www.kernel.org/"
license=('GPL2')
makedepends=('xmlto' 'docbook-xsl' 'kmod' 'inetutils' 'bc' 'git' 'uboot-tools' 'vboot-utils' 'dtc')
options=('!strip')
source=(
        # "http://cdn.kernel.org/pub/linux/kernel/v6.x/${_srcname}.tar.xz"
        "https://mirror.bjtu.edu.cn/kernel/linux/kernel/v6.x/${_srcname}.tar.xz"
        'sun50i-h6-tqc-a01.dts'
        # custom
        '0001-make-proc-cpuinfo-consistent-on-arm64-and-arm.patch'
        # emmc
	'0012-fix-h6-emmc.patch'
	'arm64-dts-allwinner-sun50i-h6-Fix-H6-emmc.patch'
        # ethernet
	'clk-gate-add-support-for-regmap-based-gates.patch'
	'mfd-Add-support-for-X-Powers-AC200.patch'
	'net-phy-Add-support-for-AC200-EPHY.patch'
	'mfd-Add-support-for-X-Powers-AC200-EPHY-syscon.patch'
	'arm64-dts-allwinner-h6-Add-AC200-EPHY-nodes.patch'
        # 'net-stmmac-sun8i-Add-support-for-enabling-a-regulator-for-PHY-I.patch'
        # 'net-stmmac-sun8i-Rename-PHY-regulator-variable-to-regulator_phy.patch'
        # 'net-stmmac-sun8i-Use-devm_regulator_get-for-PHY-regulator.patch'
        # hdmi sound
	'drv-gpu-drm-sun4i-Add-HDMI-audio-sun4i-hdmi-encoder.patch'
        # audio codec
	'ASoC-AC200-Initial-driver.patch'
	'arm64-dts-allwinner-h6-add-AC200-codec-nodes.patch'
        # misc
	'nvmem-sunxi_sid-add-sunxi_get_soc_chipid-sunxi_get_serial.patch'
	'arm64-dts-sun50i-h6.dtsi-improve-thermals.patch'
	'drv-pinctrl-sunxi-pinctrl-sun50i-h6.c-GPIO-disable_strict_mode.patch'
	'0011-arm64-dts-allwinner-h6-Add-SCPI-protocol.patch'
	'0006-DO-NOT-MERGE-arm64-dts-allwinner-h6-Protect-SCP-cloc.patch'
        # cedrus
        # 'WIP-media-uapi-hevc-add-fields-needed-for-rkvdec.patch'
        # 'HACK-media-uapi-hevc-tiles-and-num_slices.patch'
        # 'Revert-net-Remove-net-ipx.h-and-uapi-linux-ipx.h-hea.patch'
        # 'drv-media-cedrus-10-bit-HEVC-support.patch'
        # 'drv-media-cedrus-Add-callback-for-buffer-cleanup.patch'
        # 'drv-media-cedrus-h264-Improve-buffer-management.patch'
        # 'drv-media-cedrus-hevc-Improve-buffer-management.patch'
        # 'drv-media-cedrus-hevc-tiles-hack.patch'

        'config'
        'linux.preset')

[[ ${pkgver##*.} != 0 ]] && \
# source+=("https://cdn.kernel.org/pub/linux/kernel/v6.x/patch-${pkgver}.xz")
source+=("https://mirror.bjtu.edu.cn/kernel/linux/kernel/v6.x/patch-${pkgver}.xz")

md5sums=('612a9feef07be8663098a0a58cddf7a6'
         '50a0256815fb5b6296bea4f079586f79'
         '7a18066683f3351b2bbd2653db783f80'
         '6c58c6697e1275038acf579251c69d31'
         '769d375cb73370e910b1294300347c59'
         '3dad47553613977329124eb15182d7a8'
         '85b06c469167d2333fcb47368f7d9911'
         '6c2167604d7893a03dd38a0019d1a61b'
         '8509666f913425756ee2acafeb83de27'
         'fbaa4c4727f3aba25ffc024b9551ee82'
         '0f59975e5cc1d4a4e0eaa7e8fb98de1e'
         '7208f34f139345bc4d52b1cc33c2674d'
         'd87eaae6f6e58c16d8ea34b054810010'
         'dab3d3f12c095655558d94cac2aec813'
         'e89c2390097d726816c95074e60e16bf'
         'bc7537778666a8e346e2949d6c84ef42'
         '554fd010bfcfd195deeaaa5a180499b9'
         'fac6a4de3708bb8a2423a12a592120bb'
         'c5e00859db91fe7d95307dedf62e51e8'
         '66e0ae63183426b28c0ec0c7e10b5e16'
         '4fc8e83d507486c28ca1a27f1856dd21')

prepare() {
  cd ${_srcname}

  echo "Setting version..."
  echo "-$pkgrel" > localversion.10-pkgrel
  echo "${pkgbase#linux}" > localversion.20-pkgname

  # add upstream patch
  [[ ${pkgver##*.} != 0 ]] && \
  patch -p1 < "../patch-${pkgver}"

  # dts for TQC-A01
  target_dts="sun50i-h6-tqc-a01.dts"
  echo "dtb-\$(CONFIG_ARCH_SUNXI) += ${target_dts//dts/dtb}" >> "./arch/arm64/boot/dts/allwinner/Makefile"
  cat "${srcdir}/${target_dts}" > "./arch/arm64/boot/dts/allwinner/${target_dts}"

  # patches
  patch -p1 < ../0001-make-proc-cpuinfo-consistent-on-arm64-and-arm.patch

  patch -p1 < ../0012-fix-h6-emmc.patch
  patch -p1 < ../arm64-dts-allwinner-sun50i-h6-Fix-H6-emmc.patch

  patch -p1 < ../clk-gate-add-support-for-regmap-based-gates.patch
  patch -p1 < ../mfd-Add-support-for-X-Powers-AC200.patch
  patch -p1 < ../mfd-Add-support-for-X-Powers-AC200-EPHY-syscon.patch
  patch -p1 < ../net-phy-Add-support-for-AC200-EPHY.patch
  patch -p1 < ../arm64-dts-allwinner-h6-Add-AC200-EPHY-nodes.patch

  patch -p1 < ../arm64-dts-allwinner-h6-add-AC200-codec-nodes.patch

  patch -p1 < ../nvmem-sunxi_sid-add-sunxi_get_soc_chipid-sunxi_get_serial.patch
  patch -p1 < ../arm64-dts-sun50i-h6.dtsi-improve-thermals.patch
  patch -p1 < ../drv-pinctrl-sunxi-pinctrl-sun50i-h6.c-GPIO-disable_strict_mode.patch
  patch -p1 < ../0011-arm64-dts-allwinner-h6-Add-SCPI-protocol.patch
  patch -p1 < ../0006-DO-NOT-MERGE-arm64-dts-allwinner-h6-Protect-SCP-cloc.patch

  cat "${srcdir}/config" > ./.config
}

build() {
  cd ${_srcname}

  # get kernel version
  make prepare
  make -s kernelrelease > version

  make oldconfig # using old config from previous kernel version

  # build!
  unset LDFLAGS
  make ${MAKEFLAGS} Image Image.gz modules
  # Generate device tree blobs with symbols to support applying device tree overlays in U-Boot
  make ${MAKEFLAGS} DTC_FLAGS="-@" dtbs
}

_package() {
  pkgdesc="The Linux Kernel and modules - ${_desc}"
  depends=('coreutils' 'kmod' 'mkinitcpio>=0.7')
  optdepends=('wireless-regdb: to set the correct wireless channels of your country'
              'firmware-tqc-a01: firmware for TQC A01 WiFi/Bluetooth')
  provides=("linux=${pkgver}" "KSMBD-MODULE" "WIREGUARD-MODULE")
  conflicts=('linux')
  install=${pkgname}.install

  cd ${_srcname}
  local kernver="$(<version)"
  local modulesdir="$pkgdir/usr/lib/modules/$kernver"

  echo "Installing boot image and dtbs..."
  install -Dm644 arch/arm64/boot/Image{,.gz} -t "${pkgdir}/boot"
  make INSTALL_DTBS_PATH="${pkgdir}/boot/dtbs" dtbs_install

  echo "Installing modules..."
  make INSTALL_MOD_PATH="$pkgdir/usr" INSTALL_MOD_STRIP=1 DEPMOD=/doesnt/exist modules_install

  # remove build link
  rm "$modulesdir"/build

  # sed expression for following substitutions
  local _subst="
    s|%PKGBASE%|${pkgbase}|g
    s|%KERNVER%|${kernver}|g
  "

  # install mkinitcpio preset file
  sed "${_subst}" ../linux.preset |
    install -Dm644 /dev/stdin "${pkgdir}/etc/mkinitcpio.d/${pkgbase}.preset"

  # rather than use another hook (90-linux.hook) rely on mkinitcpio's 90-mkinitcpio-install.hook
  # which avoids a double run of mkinitcpio that can occur
  install -d "${pkgdir}/usr/lib/initcpio/"
  echo "dummy file to trigger mkinitcpio to run" > "${pkgdir}/usr/lib/initcpio/$(<version)"
}

_package-headers() {
  pkgdesc="Header files and scripts for building modules for linux kernel - ${_desc}"
  provides=("linux-headers=${pkgver}")
  conflicts=('linux-headers')

  cd $_srcname
  local builddir="$pkgdir/usr/lib/modules/$(<version)/build"

  echo "Installing build files..."
  install -Dt "$builddir" -m644 .config Makefile Module.symvers System.map \
    localversion.* version vmlinux
  install -Dt "$builddir/kernel" -m644 kernel/Makefile
  install -Dt "$builddir/arch/arm64" -m644 arch/arm64/Makefile
  cp -t "$builddir" -a scripts

  # add xfs and shmem for aufs building
  mkdir -p "$builddir"/{fs/xfs,mm}

  echo "Installing headers..."
  cp -t "$builddir" -a include
  cp -t "$builddir/arch/arm64" -a arch/arm64/include
  install -Dt "$builddir/arch/arm64/kernel" -m644 arch/arm64/kernel/asm-offsets.s
  mkdir -p "$builddir/arch/arm"
  cp -t "$builddir/arch/arm" -a arch/arm/include

  install -Dt "$builddir/drivers/md" -m644 drivers/md/*.h
  install -Dt "$builddir/net/mac80211" -m644 net/mac80211/*.h

  # https://bugs.archlinux.org/task/13146
  install -Dt "$builddir/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

  # https://bugs.archlinux.org/task/20402
  install -Dt "$builddir/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
  install -Dt "$builddir/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
  install -Dt "$builddir/drivers/media/tuners" -m644 drivers/media/tuners/*.h

  # https://bugs.archlinux.org/task/71392
  install -Dt "$builddir/drivers/iio/common/hid-sensors" -m644 drivers/iio/common/hid-sensors/*.h

  echo "Installing KConfig files..."
  find . -name 'Kconfig*' -exec install -Dm644 {} "$builddir/{}" \;

  echo "Removing unneeded architectures..."
  local arch
  for arch in "$builddir"/arch/*/; do
    [[ $arch = */arm64/ || $arch == */arm/ ]] && continue
    echo "Removing $(basename "$arch")"
    rm -r "$arch"
  done

  echo "Removing documentation..."
  rm -r "$builddir/Documentation"

  echo "Removing broken symlinks..."
  find -L "$builddir" -type l -printf 'Removing %P\n' -delete

  echo "Removing loose objects..."
  find "$builddir" -type f -name '*.o' -printf 'Removing %P\n' -delete

  echo "Stripping build tools..."
  local file
  while read -rd '' file; do
    case "$(file -bi "$file")" in
      application/x-sharedlib\;*)      # Libraries (.so)
        strip -v $STRIP_SHARED "$file" ;;
      application/x-archive\;*)        # Libraries (.a)
        strip -v $STRIP_STATIC "$file" ;;
      application/x-executable\;*)     # Binaries
        strip -v $STRIP_BINARIES "$file" ;;
      application/x-pie-executable\;*) # Relocatable binaries
        strip -v $STRIP_SHARED "$file" ;;
    esac
  done < <(find "$builddir" -type f -perm -u+x ! -name vmlinux -print0)

  echo "Adding symlink..."
  mkdir -p "$pkgdir/usr/src"
  ln -sr "$builddir" "$pkgdir/usr/src/$pkgbase"
}

pkgname=("${pkgbase}" "${pkgbase}-headers")
for _p in ${pkgname[@]}; do
  eval "package_${_p}() {
    $(declare -f "_package${_p#$pkgbase}")
    _package${_p#${pkgbase}}
  }"
done
